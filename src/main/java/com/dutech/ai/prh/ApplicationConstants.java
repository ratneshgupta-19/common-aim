package com.dutech.ai.prh;

public final class ApplicationConstants {

	public static final String DEFAULT_LOCALE = "en";
	public static final String SUCCESS = "Success";
	/**Constansts used for risk assessment logic**/
	public static final String INSPECTION_ACTION_NA = "NA";
	public static final String INSPECTION_ACTION_DV = "DV";
	public static final String INSPECTION_ACTION_PI = "PI";
	public static final String INSPECTION_ACTION_SI = "SI";
	public static final String INSPECTION_ACTION_PCA = "PCA";
	public static final String INVOICE_DECISION_KEY = "invoice";
	public static final String ITEM_DECISION_KEY = "items";
	public static final String DECLARATION_DECISION_KEY = "declaration";
	public static final String CREATEDBY = "SYSTEM";
	public static final String RISK_ASS_STATUS = "Pending";
	public static final String RISK_ASS_STATUS_COMPLETED = "Completed";

}
