package com.dutech.ai.prh.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.ToString;
import me.alidg.errors.annotation.ExceptionMapping;
import me.alidg.errors.annotation.ExposeAsArg;

/**
 * An example of an application specific Business Exception.
 * <ol>
 * <li>Annotate with @ExceptionMapping, passing the errorCode to resolve</li>
 * <li>The statusCode field MUST be set to CONFLICT for all Business Exceptions</li>
 * <li>Utilize Lombok to auto-generate constructor() and toString() with @AllArgsConstructor and @ToString</li>
 * <li>Declare the exception arguments and annotate with @ExposeAsArgs</li>
 * <li>Create an entry in messages.properties for the errorCode defined</li>
 * </ol>
 * 
 * @author Ameer Qudsiyeh
 */
@SuppressWarnings("serial")
@AllArgsConstructor @ToString
@ExceptionMapping(statusCode = HttpStatus.CONFLICT, errorCode = "NotFound.employee")
public class EmployeeNotFoundException extends RuntimeException {

	@ExposeAsArg(0) private final Long id;
}
