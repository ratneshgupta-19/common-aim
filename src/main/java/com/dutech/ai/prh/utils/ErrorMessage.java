package com.dutech.ai.prh.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorMessage {
	
	private String errorCode;
	
	private String errorMessage;
	
	private String detailErrorMessage;
	
	public ErrorMessage(String errorCode, String errorMessage) {
		this.errorCode=errorCode;
		this.errorMessage= errorMessage;
	}
	
	

}
