package com.dutech.ai.prh.utils;

import java.util.stream.Stream;

public enum ErrorCode {
	
	VPINVALIDTHRESHOLD("VP-ERR-100", "valuationprofile.invalid.threshold"),
	VPDUPLICATEPROFILECODE("VP-ERR-101", "valuationprofile.duplicate.profile.code"),
	VPRECORDNOTSAVED("VP-ERR-102", "valuationprofile.record.not.save"),
	VPNORECORDFOUND("VP-ERR-103", "valuationprofile.record.not.found"),
	VPNORECORDFOUNDWITHGIVENID("VP-ERR-104", "valuationprofile.record.not.found.for.id"),
	VPINTERNALSERVERERROR("VP-ERR-105", "valuationprofile.internal.server.error"),
	VPPROFILECODENOTGENERATED("VP-ERR-106", "valuationprofile.profilecode.generation.error"),
	VPINVALIDINPUT("VP-ERR-107", "valuationprofile.invalid.input"),
	VPNOHISTORYRECORDFOUNDWITHGIVENID("VP-ERR-108", "valuationprofile.history.record.not.found.for.id"),
	VP_FROM_DATE_NOT_VALID("VP-ERR-109", "valuationprofile.from.date.not.valid"),
	VP_TO_DATE_NOT_VALID("VP-ERR-110", "valuationprofile.to.date.not.valid"),
	NO_RESPONSE("CV-ERR-101","CV.response"),
	VALIDATION_FAILED("CV-ERR-102","CV.Validation.Failed"),
	INTERNALSERVER_ERROR("CV-ERR-103","Internal.Server.Error"),
	NOTFOUND_PROFILE("CV-ERR-104","Profile.not.found"), NOTFOUND("CV-ERR-105","Record.not.found");
	
	private String shortDescription;
	private String description;
	private String code;
	
	
	private ErrorCode(String code, String shortDesc, String description) {
	    this.code = code;
	    this.description = description;
	    this.shortDescription=shortDesc;
	  }
	
	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	private ErrorCode(String code, String shortDesc) {
	    this.code = code;
	    this.shortDescription=shortDesc;
	  }
	public static Stream<ErrorCode> stream() {
        return Stream.of(ErrorCode.values()); 
    }
}
