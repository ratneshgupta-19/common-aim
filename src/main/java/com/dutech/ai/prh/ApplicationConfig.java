package com.dutech.ai.prh;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

/**
 * Application-specific configuration, which may include Beans, Initializations, etc.
 */
@Configuration
@Slf4j
public class ApplicationConfig {
	
	 @Bean("threadPoolTaskExecutor")
	    public TaskExecutor getAsyncExecutor() {
	        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	        executor.setCorePoolSize(20);
	        executor.setMaxPoolSize(1000);
	        executor.setWaitForTasksToCompleteOnShutdown(true);
	        executor.setThreadNamePrefix("RiskProfile-");
	        return executor;
	    }
	
	
}