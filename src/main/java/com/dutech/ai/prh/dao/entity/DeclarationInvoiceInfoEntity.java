package com.dutech.ai.prh.dao.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DECL_INVOICE_INFO")
public class DeclarationInvoiceInfoEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DECL_INVOICE_INFO_ID")
	private Integer declInvoiceInfoId;

	protected Long declarationId;

	protected Integer sequenceNo;

	protected String invoiceNo;

	protected String invoiceDate;

	protected String incotermCode;

	protected BigDecimal invoiceValueForiegnCurrency;

	protected String currencyCode;

	protected String currencyRate;

	protected BigDecimal insuranceCost;

	protected BigDecimal frieghtCost;

	protected BigDecimal totalCIFLocalCurrency;

}
