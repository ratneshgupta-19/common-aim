package com.dutech.ai.prh.dao.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DECL_CONTAINER_INFO")
public class DeclarationContainerInfoEntity implements Serializable {
	
	private static final long serialVersionUID = -1938732904416414154L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DECL_CONTAINER_INFO_ID")
	private Integer declContainerInfoId;

	protected Long declarationId;

	protected Integer sequenceNo;

	protected String containerNo;

	protected String containerTypeSize;

	protected String sealType;

	protected String numberOfSeals;

	protected String sealNumbers;

	protected BigDecimal grossWeight;

	protected BigDecimal totalPackages;

	protected Integer totalNumberOfContainers;

	protected String uniqueConsignmentRefNo;

}
