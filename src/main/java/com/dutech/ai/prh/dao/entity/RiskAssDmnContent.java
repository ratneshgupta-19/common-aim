package com.dutech.ai.prh.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RISK_ASS_DMN_CONTENT")
public class RiskAssDmnContent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RISK_ASS_DMN_CONTENT_ID")
	private Integer riskAssDmnContentId;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date updatedDate;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date validFrom;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date validTo;
	
	protected Integer version;
	
	protected String name;
	
	protected String name2;
	
	protected String code;
	
	protected String functionCode;
	
	protected String updatedBy;
}
