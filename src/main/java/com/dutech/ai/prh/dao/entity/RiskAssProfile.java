package com.dutech.ai.prh.dao.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RISK_ASS_PROFILE")
public class RiskAssProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RISK_ASS_PROFILE_ID")
	private Integer riskAssProfileId;

	private String trs;
	private Long ruleId;
	private String category;
	private String riskIndicator;
	private String ruleDetails;
	private Integer score;
	private String riskType;
	private String recomendedAction;
	private String riskRuleOwner;
	private String inspectionCaseAction;
	private String ruleCode;
	private Integer thresholdCode;
	
	private Integer finalScore;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "RISK_ASS_PRO_ID")
	List<RiskAssItemDetails> riskAssItemsDetailsList;
}
