package com.dutech.ai.prh.dao.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DECL_INFO")
public class DeclarationInfoEntity implements Serializable{

	private static final long serialVersionUID = 1307889765677453271L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DECL_INFO_ID")
	private Integer declInfoId;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date acceptanceDateTime;
	  
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date cancellationDateTime;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date effectiveDateTime;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date expirationDateTime;

	protected String clientReferenceNumber;

	protected String declarationNumber;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date declarationIssueDateTime;

	protected String declarationIssueLocationID;

	protected String languageCode;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date rejectionDateTime;

	protected String declarationType;

	protected String documentStatus;

	protected String customsOfficeCode;

	protected String regimeType;

	protected String documentVersionNo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DECL_INFOR_ID")
	protected Set<SupportingDocumentInfoEntity> supportingDocumentList;

	protected String transportMode;

	protected String declarantCode;

	protected Integer declarationId;

	protected String declarantName;

	protected String submitterCode;

	protected String submitterName;

	protected String submitterRemarks;

	protected String reviewerCode;

	protected String reviewerName;

	protected String reviewerRemarks;

	protected String brokerCode;

	protected String brokerName;

	protected String payMethodCode;

	protected String payMethodName;

	protected String overallStatus;
    protected String remarks;
	protected String further_action;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="Shipping_Info")
	protected ShippingInfoEntity shippingInfo;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DECL_INFOR_ID")
	protected Set<DeclarationInvoiceInfoEntity> declarationInvoiceInfoList;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DECL_INFOR_ID")
	protected Set<GoodDetailsInfoEntity> goodDetailsInfoList;

}
