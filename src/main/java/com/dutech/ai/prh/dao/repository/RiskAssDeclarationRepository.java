package com.dutech.ai.prh.dao.repository;



import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import com.dutech.ai.prh.dao.entity.QRiskAssDeclaration;
import com.dutech.ai.prh.dao.entity.RiskAssDeclaration;
import com.querydsl.core.types.dsl.StringPath;
/**
 *  Repository for the AEO Request Header Entity.
 * The JpaRepository provides basic CRUD operations in addition to Paging an Sorting.
 * The QueryDsl classes enhance the repository with filtering capabilities
 * 
 * @author Gaurav
 */
public interface RiskAssDeclarationRepository 
extends JpaRepository<RiskAssDeclaration,Long>,QuerydslPredicateExecutor<RiskAssDeclaration>, QuerydslBinderCustomizer<QRiskAssDeclaration>{
	/**
	 * QueryDsl customization. Triggered on filtered find operations.
	 */
	@Override
	default void customize(QuerydslBindings bindings, QRiskAssDeclaration root) {
		// Exclude the Id from query filter
		bindings.excluding(root.riskAssDecId);

		// Perform IgnoreCase & EqualsLike comparison
		bindings.bind(String.class).first(
				(StringPath path, String value) -> path.containsIgnoreCase(value)
		);		
	}
	public Optional<RiskAssDeclaration> findByRiskAssDecId(Integer riskAssId);
	public Optional<RiskAssDeclaration> findTopByOrderByRiskAssDecIdDesc();	
}
