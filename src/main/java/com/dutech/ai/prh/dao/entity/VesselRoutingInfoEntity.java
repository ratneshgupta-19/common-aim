package com.dutech.ai.prh.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "VESSEL_ROUT_INFO")
public class VesselRoutingInfoEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "VESSEL_ROUT_INFO_ID")
	private Integer vesselRoutInfoId;

	protected Integer sequenceNo;

	protected Long declarationId;

	protected String portType;

	protected String portCode;

	protected String portName;

	protected String purposeOfCall;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date arrivalDateTime;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date departureDateTime;

	protected String transitTranportId;

	protected String transitTranportName;

	protected String transitTranportNo;

	protected String transitTranportTripNo;

	protected String transitTranportMode;

	protected String faciltyType;

	protected String faciltyCode;

	protected String faciltyName;

}
