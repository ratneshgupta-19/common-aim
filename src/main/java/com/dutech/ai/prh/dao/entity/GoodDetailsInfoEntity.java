package com.dutech.ai.prh.dao.entity;



import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "GOODS_DETAILS_INFO")
public class GoodDetailsInfoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "GOODS_DTL_INFO_ID")
	private Integer goodsDtlInfoId;

	protected Long declarationId;

	protected Integer sequenceNo;

	protected String hsCode;

	protected String productCode;

	protected String goodsDescription;

	protected String gooodsCondition;

	protected String countryOfOrigin;

	protected BigDecimal goodsQuantity;

	protected String quantityUOM;

	protected BigDecimal netWeight;

	protected BigDecimal grossWeight;

	protected BigDecimal declaredValue;

	protected BigDecimal customsValue;

	protected String valuationMethod;

	protected String exemptionType;

	protected String dutyTypeCode;

	protected BigDecimal dutyValue;

	protected BigDecimal unitDuty;

	protected Integer invoiceLineItemSequence;

    protected String valuationStatus;
}
