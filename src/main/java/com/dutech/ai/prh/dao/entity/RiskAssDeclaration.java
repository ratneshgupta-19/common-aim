package com.dutech.ai.prh.dao.entity;



import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import com.dutech.ai.prh.dao.entity.RiskAssProfile;
import com.dutech.ai.prh.dao.entity.RiskAssDeclarationThreatSummary;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RISK_ASS_DECLARATION")
public class RiskAssDeclaration implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RISK_ASS_DEC_ID")
	private Integer riskAssDecId;

	@Column(length = 100)
	private String referenceCode;

	private Long referanceId;

	private String importer;

	private String AgentCode;

	private String Status;

	private String regime;

	private String procedure;

	private String modeOfTransport;

	private Integer noOfLines;

	private Integer noOfContainers;

	private String goodsLocation;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate createdDate;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate updatedDate;
	
	private String createdBy;
	
	private String updatedBy;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate dateOfarrival;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "RISK_ASS_DECL_ID")
	List<RiskAssProfile> riskAssProfileList;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "RISK_ASS_DECL_ID")
	List<RiskAssDeclarationThreatSummary> riskAssDeclarationThreatSummary;
	
	private Integer ipr;
	
	private Integer rev;
	
	private Integer val;
	
	private Integer random;
	
	private String inspectionAction;
}
