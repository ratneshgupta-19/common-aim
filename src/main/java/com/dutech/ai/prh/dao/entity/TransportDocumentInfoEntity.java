package com.dutech.ai.prh.dao.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TRANS_DOC_INFO")
public class TransportDocumentInfoEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRANS_DOC_INFO_ID")
	private Integer transDocInfoId;

	protected Integer sequenceNo;

	protected Long declarationId;

	protected String documentType;

	protected String cargoType;

	protected String marksNo;

	protected BigDecimal netWeight;

	protected BigDecimal grossWeight;

	protected BigDecimal totalPackages;

	protected Integer totalNumberOfContainers;

	protected String uniqueConsignmentRefNo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "TRANS_DOC_INFOR_ID")
	protected List<DeclarationContainerInfoEntity> DeclarationContainerInfoList;

}
