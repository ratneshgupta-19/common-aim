package com.dutech.ai.prh.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SUPPORTING_DOC_INFO")
public class SupportingDocumentInfoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SUPPORTING_DOC_INFO_ID")
	private Integer supportDocInfoId;

	protected Integer sequenceNumber;

	protected Long declarationId;

	protected String documentTypeCode;

	protected String documentName;

	protected String documentReferenceNumber;

	protected String binaryFileName;

	protected String binaryFileType;

	protected String binaryFileExtension;

	protected String binaryFileSize;

}
