package com.dutech.ai.prh.dao.repository;



import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import com.dutech.ai.prh.dao.entity.DeclarationInfoEntity;
import com.dutech.ai.prh.dao.entity.QDeclarationInfoEntity;
import com.querydsl.core.types.dsl.StringPath;
/**
 *  Repository for the Request  Entity.
 * The JpaRepository provides basic CRUD operations in addition to Paging an Sorting.
 * The QueryDsl classes enhance the repository with filtering capabilities
 * 
 * @author Gaurav
 */
public interface DeclarationInfoRepository 
extends JpaRepository<DeclarationInfoEntity,Long>,QuerydslPredicateExecutor<DeclarationInfoEntity>, QuerydslBinderCustomizer<QDeclarationInfoEntity>{
	/**
	 * QueryDsl customization. Triggered on filtered find operations.
	 */
	@Override
	default void customize(QuerydslBindings bindings, QDeclarationInfoEntity root) {
		// Exclude the Id from query filter
		bindings.excluding(root.declInfoId);

		// Perform IgnoreCase & EqualsLike comparison
		bindings.bind(String.class).first(
				(StringPath path, String value) -> path.containsIgnoreCase(value)
		);		
	}
	public Optional<DeclarationInfoEntity> findByDeclInfoId(Integer declInfoId);
	public Optional<DeclarationInfoEntity> findTopByOrderByDeclInfoId();
	public Optional<DeclarationInfoEntity> findByDeclarationId(Integer declarationId);	
	
	
}
