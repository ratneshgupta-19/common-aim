package com.dutech.ai.prh.dao.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Gaurav Verma
 * 
 */
@Data
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SHIPPING_INFO")
public class ShippingInfoEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SHIPPING_INFO_ID")
	private Integer shippingInfoId;

	protected Long declarationId;

	protected String transportTripNo;

	protected String transportNo;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date arrivalDate;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	protected Date departureDate;

	protected String portOfEntryCode;

	protected String portOfEntryName;

	protected String portOfExitCode;

	protected String portOfExitName;

	protected String destinationPortCode;

	protected String destinationPortName;

	protected String transitTranportId;

	protected String transitTranportName;

	protected String transitTranportNo;

	protected String transitTranportTripNo;

	protected String transitTranportMode;

	protected String faciltyType;

	protected String faciltyCode;

	protected String faciltyName;
		
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "SHIPPING_INFOR_ID")
	protected List<VesselRoutingInfoEntity> VesselRoutingInfoList;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "SHIPPING_INFOR_ID")
	protected List<TransportDocumentInfoEntity> TranportDocumentInfoList;

}
