package com.dutech.ai.prh.support.client;

/**
 * Application wide constants
 * 
 * @author Bojjanna.Vemula
 */
public final class ClientConstants {

   
	public static final String DES = "des";

    public static final String DES_EVALUATE_URI = "/api/v1/decisions/decisionDefinitionkey/%s/evaluate";
    
    public static final String DES_CREATE_DMN = "/api/v1/decisions/create/?autoDeployFlag=%s&dmnName=%s1";

    public static final String BPS = "bps";
    
    public static final String DECLARATION = "declaration";
    
    public static final String BPS_PROCESS_URI = "/api/v1/processes/key/%s/versionTag/1.0/start?businessKey=%s";
    
    public static final String DECLARATION_PROCESS_URI = "/api/v1/declarations/risk/%s";

    
    public static final String DES_UMS_PERSISTANCE_AMMENDE = "umsPersistanceAmend";
    
    public static final String BPS_RISK_PROFILE = "riskProfileCreation";
    
    public static final String GET_DECL = "getDeclaration";
    
    public static final String BASE_URL = "/intelligenceman/api/v1/riskprofile/search?profileId=";
    
    public static final String RISK_PROFILE_ROLE = "intelligenceofficer";
    
    public static final String EMAIL_ID_CC = "riyasat.ali@niit-tech.com";
    public static final String EMAIL_ID_TO = "Subject";
    public static final String SUBJECT = "Subject";
    public static final String BODY = "Profile Submitted";
    public static final String RECIEVER = "riyasat.ali@niit-tech.com";
    
    public static final String CLM_USER__PROFILES_BY_USERID = "/api/v1/ums/user/userProfiles?loginIds=%s";


    public static final String CLM = "clm";
    public static final String CLM_USER_VALIDATE_URI = "/api/v1/ums/user/validate/%s";
    public static final String RDM_TYPES_FIND_BY_ID_URI = "/api/v1/types/%s";
    public static final String RDM = "rdm";
    public static final String RDM_VALUES_FIND_BY_RREFTYPE_URI = "/api/v1/values/refType/%s";
    public static final String RDM_TYPES_VALIDATE_URI = "/api/v1/values/validate";
    public static final String RDM_LABELS_FIND_BY_CODE_URI = "/api/v1/values/refType/labels?refTypes=%s&codes=%s";

}
