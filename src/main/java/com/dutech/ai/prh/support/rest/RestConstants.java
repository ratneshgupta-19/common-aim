package com.dutech.ai.prh.support.rest;

/**
 * Application wide constants
 * 
 * @author Ameer Qudsiyeh
 *
 */
public final class RestConstants {
	public static final String API_BASE 			= "/api/";
	
    public static final String PREAUTH_HEADER_LABEL = "X-USERID";
    public static final String PREAUTH_USER_ROLE 	= "ROLE_USER";
    
    public static final String LOCALE_QUERYPARAM 	= "lang";
    
}