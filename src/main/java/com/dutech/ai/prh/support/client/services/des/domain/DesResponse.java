package com.dutech.ai.prh.support.client.services.des.domain;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DesResponse {
	List<Map<String, Object>> resultList;
}
