package com.dutech.ai.prh.support.repository;

import java.util.function.Predicate;

import com.dutech.ai.prh.support.resource.Resource;



public interface ResourceRepository<T extends Resource> {

    T save(T resource);

    T update(T resource);

    void delete(T resource);

    void delete(Predicate<T> predicate);

    T find(T resource);

    T find(Predicate<T> predicate);

    Iterable<T> saveAll(Iterable<T> resources);

    void deleteAll(Iterable<T> resources);

    void deleteAll(Predicate<T> predicate);

    Iterable<T> findAll();

    Iterable<T> findAll(Predicate<T> predicate);
}
