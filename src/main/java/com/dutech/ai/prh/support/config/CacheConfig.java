package com.dutech.ai.prh.support.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * Application Cache configuration. SpringBoot will auto-bootstrap the CacheManager depending on the property spring.cache.type 
 * and the existence of a supported Cache Provider on the Classpath.
 * 
 * @author Ameer Qudsiyeh
 */
@Configuration
@EnableCaching
public class CacheConfig {

}