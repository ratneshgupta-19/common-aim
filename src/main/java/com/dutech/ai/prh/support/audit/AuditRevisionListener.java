package com.dutech.ai.prh.support.audit;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * This listener enhances Envers with the User (Prinicipal) who initiated the database action. The 
 * Principal is extracted from the SecurityContext.
 * 
 * @author Ameer Qudsiyeh
 */
public class AuditRevisionListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {		
		Authentication authCtx = SecurityContextHolder.getContext().getAuthentication();
		String name = authCtx != null && authCtx.isAuthenticated() 
				? authCtx.getName() 
				: "--anonymous--";
		
		AuditRevisionEntity audit = (AuditRevisionEntity) revisionEntity;
        audit.setPrincipal(name);
	}

}
