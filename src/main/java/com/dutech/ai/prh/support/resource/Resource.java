package com.dutech.ai.prh.support.resource;

import java.net.URI;

import lombok.Data;

@Data
public abstract class Resource<T> {
    protected String name;

    protected URI uri;

    protected T data;
}
