package com.dutech.ai.prh.support.audit;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * Envers base entity
 * 
 * @author Ameer Qudsiyeh
 */
@Entity
@Table(name = "audit_revisions")
@RevisionEntity(AuditRevisionListener.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter @Setter
@SuppressWarnings("serial")
public class AuditRevisionEntity extends DefaultRevisionEntity {

	private String principal;
}
