package com.dutech.ai.prh.support.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ModelMapper configuration. Refer to documentation at http://modelmapper.org/
 * 
 * @author Ameer Qudsiyeh
 */
@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper mapper() {
        ModelMapper modelMapper = new ModelMapper();
        
        // Configure the ModelMapper
        modelMapper.getConfiguration()
            .setDeepCopyEnabled(true)
            .setSkipNullEnabled(true);

        return modelMapper;
    }
}