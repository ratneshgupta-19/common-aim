package com.dutech.ai.prh.support.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * JPA configuration, providing an AuditAware bean.
 * 
 * @author Ameer Qudsiyeh
 */
@Configuration
@EnableTransactionManagement
//@EnableJpaRepositories(repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class)
public class JpaConfig {

}
