package com.dutech.ai.prh.support.client.services.des.service.impl;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dutech.ai.prh.api.v1.model.dto.DmnDto;
import com.dutech.ai.prh.support.client.ClientConstants;
import com.dutech.ai.prh.support.client.services.des.domain.DesResponse;
import com.dutech.ai.prh.support.client.services.des.service.DesService;
import com.dutech.ai.prh.support.exception.ResourceException;
import com.dutech.ai.prh.support.repository.ResourceRepository;
import com.dutech.ai.prh.support.resource.RestResource;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DesServiceImpl implements DesService {

    private ResourceRepository<RestResource> resourceRepository;

    @Autowired
    public DesServiceImpl(ResourceRepository<RestResource> resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    /**
     * Provides method to interface with other micro services
     * 
     * @author Vemula Bojjanna
     */

	@Override
	public DesResponse evaluate(Map<String, Object> request, String... arg) {
        log.debug("Fetching Data from for evaluate :" + request);
        RestResource resource = RestResource.run(ClientConstants.DES, ClientConstants.DES_EVALUATE_URI, request, arg);
        RestResource restResourceApp = resourceRepository.save(resource);

        ObjectMapper objectMapper = new ObjectMapper();

        DesResponse response = null;
        try {

            log.debug("Des Response :" + restResourceApp.getData());
            response = objectMapper.readValue((String) restResourceApp.getData(), DesResponse.class);
        	log.debug("Response object from ofbiz response in evaluate:" + response);            
        } catch (JsonParseException e) {
        	 log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        } catch (JsonMappingException e) {
        	log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        } catch (IOException e) {
        	log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        }
        return response;
    }
	
	@Override
	public <T> DesResponse evaluateData(T request, String... arg) {
        RestResource resource = RestResource.run(ClientConstants.DES, ClientConstants.DES_EVALUATE_URI, request, arg);
        log.debug("Fetching Data from {} for evaluate with request {} :" + resource.getUri(),request);
        RestResource restResourceApp = resourceRepository.save(resource);

        ObjectMapper objectMapper = new ObjectMapper();

        DesResponse response = null;
        try {

            log.debug("Des Response :" + restResourceApp.getData());
            response = objectMapper.readValue((String) restResourceApp.getData(), DesResponse.class);
        	log.debug("Response object from response to evaluate:" + response);            
        } catch (JsonParseException e) {
        	 log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        } catch (JsonMappingException e) {
        	log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        } catch (IOException e) {
        	log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        }
        return response;
    }
	
	/**
     * Provides method to interface with other micro services
     * 
     * @author Archna
     */

	@Override
	public DesResponse evaluateProfile(Map<String, Object> request, String... arg) {
        log.info("Arc Fetching Data from for evaluate :" + request);
        RestResource resource = RestResource.run(ClientConstants.DES, ClientConstants.DES_EVALUATE_URI, request, arg);
        RestResource restResourceApp = resourceRepository.save(resource);

        ObjectMapper objectMapper = new ObjectMapper();

        DesResponse response = null;
        try {

            log.info("Des Response :" + restResourceApp.getData());
            response = objectMapper.readValue((String) restResourceApp.getData(), DesResponse.class);       	
        } catch (JsonParseException e) {
        	 log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        } catch (JsonMappingException e) {
        	log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        } catch (IOException e) {
        	log.error("ResourceException, while connect the Des for  the evaluate: " + e);
            throw new ResourceException(e);
        }
        return response;
    }
	
	@Override
	public void createDMN(DmnDto request, String... arg) {
        log.debug("submitting data to create :" + request);
        RestResource resource = RestResource.run(ClientConstants.DES, ClientConstants.DES_CREATE_DMN, request,arg);
        RestResource restResourceApp = resourceRepository.save(resource);
        log.info("Des service resposne to create dmn :" + restResourceApp.getData());

    }
    
   
}
