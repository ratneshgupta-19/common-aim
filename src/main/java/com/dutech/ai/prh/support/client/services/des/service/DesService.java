package com.dutech.ai.prh.support.client.services.des.service;

import java.util.Map;

import com.dutech.ai.prh.api.v1.model.dto.DmnDto;
import com.dutech.ai.prh.support.client.services.des.domain.DesResponse;

public interface DesService {

	DesResponse evaluate(Map<String, Object> request, String... arg);

	void createDMN(DmnDto request,String... arg);

	DesResponse evaluateProfile(Map<String, Object> request, String... arg);

	<T> DesResponse evaluateData(T request, String... arg);
	
}
