package com.dutech.ai.prh.support.repository;

import java.net.URI;
import java.util.function.Predicate;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.dutech.ai.prh.support.exception.ResourceException;
import com.dutech.ai.prh.support.resource.ResourceProperties;
import com.dutech.ai.prh.support.resource.RestResource;
import com.dutech.ai.prh.support.rest.RestConstants;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RestResourceRepository<T extends RestResource> extends AbstractResourceRepository<T> {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Override
    public T save(T resource) {
        try {
            HttpHeaders headers = initHeader(resource);
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Object> request = new HttpEntity<>(resource.getData(), headers);

            Object data;
            Class respClass = (Class) resource.getType();
            log.info("RestResourceRepository calling the external service to save data with uri::::" + resource.getUri());
            ResponseEntity response = restTemplate().exchange(resource.getUri(), HttpMethod.POST, request, respClass);
            data = response.getBody();
            resource.setData(data);

            return resource;
        } catch (HttpServerErrorException ex) {
            String response = ex.getResponseBodyAsString();
            throw new ResourceException(response, ex);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public T update(T resource) {
        try {
            HttpHeaders headers = initHeader(resource);
            HttpEntity<Object> request = new HttpEntity<>(resource.getData(), headers);

            Object data;
            Class respClass = (Class) resource.getType();
            log.info("RestResourceRepository calling the external service to update data with uri::::" + resource.getUri());
            ResponseEntity response = restTemplate().exchange(resource.getUri(), HttpMethod.PUT, request, respClass);
            data = response.getBody();
            resource.setData(data);

            return resource;
        } catch (HttpServerErrorException ex) {
            String response = ex.getResponseBodyAsString();
            throw new ResourceException(response, ex);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void delete(T resource) {
        try {
            HttpHeaders headers = initHeader(resource);
            HttpEntity<Object> request = new HttpEntity<>(headers);

            Class respClass = (Class) resource.getType();
            log.info("RestResourceRepository calling the external service to delete data with uri::::" + resource.getUri());
            ResponseEntity response = restTemplate().exchange(resource.getUri(), HttpMethod.DELETE, request, respClass);
            resource.setData(response.getBody());

        } catch (HttpServerErrorException ex) {
            String response = ex.getResponseBodyAsString();

            throw new ResourceException(response, ex);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public T find(T resource) {
        try {
            HttpHeaders headers = initHeader(resource);
            HttpEntity<Object> request = new HttpEntity<>(headers);

            Class respClass = (Class) resource.getType();
            log.info("RestResourceRepository calling the external service to fetch data with uri::::" + resource.getUri());
            ResponseEntity response = restTemplate().exchange(resource.getUri(), HttpMethod.GET, request, respClass);
            resource.setData(response.getBody());

            return resource;
        } catch (HttpServerErrorException ex) {
            String response = ex.getResponseBodyAsString();

            throw new ResourceException(response, ex);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public T find(Predicate<T> predicate) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterable<T> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterable<T> findAll(Predicate<T> predicate) {
        throw new UnsupportedOperationException();
    }

    private HttpHeaders initHeader(T resource) {
        ResourceProperties.Url url = EndPointHelper.get(resource.getName());
        HttpHeaders headers = new HttpHeaders();
        if (resource.getUri() == null) {
            URI uri = resource.getBuilder().build(url.getEndPoint());

            resource.setUri(uri);
        }
        Authentication authCtx = SecurityContextHolder.getContext().getAuthentication();
        String name = authCtx != null && authCtx.isAuthenticated() ? authCtx.getName() : url.getUsername();

        headers.set(RestConstants.PREAUTH_HEADER_LABEL, name);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Accept", MediaType.APPLICATION_JSON_UTF8.toString());
        return headers;
    }
}
