package com.dutech.ai.prh.support.persist;

import java.util.Map;

import org.springframework.http.HttpStatus;

import lombok.ToString;
import me.alidg.errors.annotation.ExceptionMapping;
import me.alidg.errors.annotation.ExposeAsArg;

/**
 * General purpose Business Exception to mark that an Entity already exists
 * 
 * @author Ameer Qudsiyeh
 */
@SuppressWarnings("serial")
@ToString
@ExceptionMapping(statusCode = HttpStatus.CONFLICT, errorCode = "Exists.Entity.message")
public class EntityAlreadyExistsException extends RuntimeException {

	@ExposeAsArg(0) private String entity;
	@ExposeAsArg(1) private String query;
	
	/**
	 * Create the exception with the Entity class and Id value
	 * @param clazz The entity Class
	 * @param id The entity id
	 */
	public EntityAlreadyExistsException(Class<?> clazz, Number id) {
		this(clazz, Map.of("id", id));
	}
	
	/**
	 * Create the exception with the Entity class and a map of query parameters
	 * @param clazz The entity class
	 * @param queryParams A map of query parameters used to find the Entity
	 * @see {@link Map.of}
	 */
	public EntityAlreadyExistsException(Class<?> clazz, Map<String, Object> queryParams) {
		super("Entity: " + clazz.getSimpleName() + " already exists for query: " + queryParams);
		this.entity = clazz.getSimpleName();
		this.query = queryParams.toString();
	}
}
