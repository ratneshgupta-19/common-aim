package com.dutech.ai.prh.support.resource;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "app.resource")
@Component
public class ResourceProperties {
    private File file;

    private List<Url> urls;

    @Getter
    @Setter
    public static class File {
        private String path;
    }

    @Getter
    @Setter
    public static class Url {
        private String name;

        private String endPoint;

        private String username;

        private String password;
    }
}
