package com.dutech.ai.prh.api.v1.model.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Harihara Sudhan
 */
@Data
@NoArgsConstructor
@ApiModel(description = "ShippingInfo")

@JsonIgnoreProperties(ignoreUnknown = true)

public class ShippingInfo  {

/**
 * * Attribute Name: Shipping Info Id(Primary Key id)
 */
protected Long shippingInfoId;
	 
/**
  * Attribute Name: Declaration Info Id(Foreign Key id)
  */

protected Long declarationId;
	
	
protected String transportTripNo;

protected String transportNo;
    
protected Date arrivalDate;

protected Date departureDate;

protected String portOfEntryCode;

protected String portOfEntryName;

protected String portOfExitCode;

protected String portOfExitName;

protected String destinationPortCode;

protected String destinationPortName;
    
protected String transitTranportId;

protected String transitTranportName;

protected String transitTranportNo;

protected String transitTranportTripNo;

protected String transitTranportMode;

protected String faciltyType;

protected String faciltyCode;

protected String faciltyName;

protected List<VesselRoutingInfo> VesselRoutingInfoList;

protected List<TransportDocumentInfo> TranportDocumentInfoList;



   
}
