package com.dutech.ai.prh.api.v1.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Harihara Sudhan
 */
@Data
@NoArgsConstructor
@ApiModel(description = "DeclarationContainerInfo")

@JsonIgnoreProperties(ignoreUnknown = true)

public class DeclarationContainerInfo  {
	

/**
 * 	 * * Attribute Name: DeclarationContainerInfo Id(Primary Key id)
 */
protected long declarationContainerInfo;
			 
/**
 * 
 * Attribute Name: TransportDocumentInfo(Foriegn Key id)
 */

protected long transportDocumentInfoId;	
	
protected int sequenceNo;

protected String containerNo;

protected String containerTypeSize;

protected String sealType;

protected String numberOfSeals;

protected String sealNumbers;

protected BigDecimal grossWeight;

protected BigDecimal totalPackages;

protected int totalNumberOfContainers;

protected String uniqueConsignmentRefNo;

protected List<DeclarationContainerInfo> DeclarationContainerInfoList;



   
}
