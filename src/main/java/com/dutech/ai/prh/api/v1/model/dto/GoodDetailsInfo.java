package com.dutech.ai.prh.api.v1.model.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Harihara Sudhan
 */
@Data
@NoArgsConstructor
@ApiModel(description = "GoodsDetailsInfo")
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoodDetailsInfo {

	/**
	 * * Attribute Name: Goods Info Id(Primary Key id)
	 */
	protected Long goodDetailsInfoId;

	/**
	 * Attribute Name: Declaration Info Id(Foreign Key id)
	 */

	protected Long declarationInfoId;
	protected Integer sequenceNo;

	protected String hsCode;

	protected String productCode;

	protected String goodsDescription;

	protected String gooodsCondition;

	protected String countryOfOrigin;

	protected BigDecimal goodsQuantity;

	protected String quantityUOM;

	protected BigDecimal netWeight;

	protected BigDecimal grossWeight;

	protected BigDecimal declaredValue;

	protected BigDecimal customsValue;

	protected String valuationMethod;

	protected String exemptionType;

	protected String dutyTypeCode;

	protected BigDecimal dutyValue;

	protected BigDecimal unitDuty;

	protected Integer invoiceLineItemSequence;
	protected String valuationStatus;
	@JsonProperty(access = Access.READ_ONLY)
	private String ruleCode;

}
