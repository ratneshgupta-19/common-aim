package com.dutech.ai.prh.api.v1.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Harihara Sudhan
 */
@Data
@NoArgsConstructor
@ApiModel(description = "SupportingDocumentInfo")

@JsonIgnoreProperties(ignoreUnknown = true)

public class SupportingDocumentInfo {
	
/**
 * * Attribute Name: Supporting Document Info Id(Primary Key id)
 */
	protected long supportingDocumentInfo;
		 
/**
  * Attribute Name: Declaration Info Id(Foreign Key id)
 */

	protected long declarationInfoId;		
	
    protected int sequenceNumber;
    
    protected String documentTypeCode;
    
    protected String documentName;
    
    protected String documentReferenceNumber;
    
    protected String binaryFileName;
    
    protected String binaryFileType;
    
    protected String binaryFileExtension;
    
    protected String binaryFileSize;

}
