package com.dutech.ai.prh.api.v1;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dutech.ai.prh.ApplicationConstants;
import com.dutech.ai.prh.api.v1.model.dto.BaseResponseDto;
import com.dutech.ai.prh.api.v1.model.dto.RiskAssDeclarationDTO;
import com.dutech.ai.prh.dao.entity.DeclarationInfoEntity;
import com.dutech.ai.prh.dao.repository.DeclarationInfoRepository;
import com.dutech.ai.prh.service.RiskAssessmentService;
import com.dutech.ai.prh.support.rest.RestConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import me.alidg.errors.HttpError;

@RestController
@RequestMapping(path = RestConstants.API_BASE + "/v1/declaration")
@Validated
@Slf4j
public class CustomValuationController {

	@Autowired
	private DeclarationInfoRepository declarationInfoRepository;
	@Autowired
	private RiskAssessmentService riskAssessmentService;  
	
	@Autowired
	private ModelMapper mapper;

	
	@ApiOperation("Create declaration info ")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = DeclarationInfoEntity.class),
			@ApiResponse(code = 400, message = "Bad Request", response = HttpError.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
			@ApiResponse(code = 403, message = "Unauthorized", response = HttpError.class) })
	@PostMapping
	public BaseResponseDto<Object> createDeclarationInfo(@RequestBody DeclarationInfoEntity declarationInfo,
			@RequestHeader(name = HttpHeaders.ACCEPT_LANGUAGE, defaultValue = ApplicationConstants.DEFAULT_LOCALE) String langCode) {
		log.info("Payload DeclarationInfo : " + declarationInfo);
		DeclarationInfoEntity savedDeclarationInfo = this.declarationInfoRepository.save(declarationInfo);
		log.info("create DeclarationInfo : " + savedDeclarationInfo);
		return BaseResponseDto.builder().statusCode(HttpStatus.OK.value()).responseDto(savedDeclarationInfo)
				.message(ApplicationConstants.SUCCESS.toString()).build();

	}
	
	@ApiOperation("Returns risk Assessment result")
	@GetMapping(value = "/riskAssV1SyncTest/{declarationId}")
	public BaseResponseDto<RiskAssDeclarationDTO> createRiskAssessDeclaration(@PathVariable Integer declarationId) throws Exception {
		log.info("Start risk Assessment for request is {}", declarationId);
		RiskAssDeclarationDTO riskAssessDeclarationDto= riskAssessmentService.calculateRiskAssDeclarationSync(declarationId);
		
		return BaseResponseDto.<RiskAssDeclarationDTO>builder()
	    		.statusCode(HttpStatus.OK.value())
	    		.content(riskAssessDeclarationDto).message(ApplicationConstants.SUCCESS.toString()).build();
	}


}
