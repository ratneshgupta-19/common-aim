package com.dutech.ai.prh.api.v1.model.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Harihara Sudhan
 */
@Data
@NoArgsConstructor
@ApiModel(description = "VesselRoutingInfo")

@JsonIgnoreProperties(ignoreUnknown = true)

public class VesselRoutingInfo  {

	
/**
 * * Attribute Name: Shipping Info Id(Foreign Key id)
 */
protected long shippingInfoId;
			 
/**
 * 
 * Attribute Name: VesselRoutingInfo(Primary Key id)
 */

protected long vesselRoutingInfoId;

	
protected int sequenceNo;

protected String portType;

protected String portCode;

protected String portName;

protected String purposeOfCall;

protected Date arrivalDateTime;

protected Date departureDateTime;

    
protected String transitTranportId;

protected String transitTranportName;

protected String transitTranportNo;

protected String transitTranportTripNo;

protected String transitTranportMode;

protected String faciltyType;

protected String faciltyCode;

protected String faciltyName;

protected List<VesselRoutingInfo> VesselRoutingInfoList;



   
}
