package com.dutech.ai.prh.api.v1.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class DesInvoiceResponseDto {
	
	private List<DesInvoiceResultListDto> resultList;

}
