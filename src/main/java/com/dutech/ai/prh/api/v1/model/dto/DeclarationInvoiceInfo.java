package com.dutech.ai.prh.api.v1.model.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Harihara Sudhan
 */
@Data
@NoArgsConstructor
@ApiModel(description = "DeclarationInvoiceInfo")

@JsonIgnoreProperties(ignoreUnknown = true)

public class DeclarationInvoiceInfo  {

	
/**
 * * Attribute Name: DeclarationInvoiceInfo Id(Primary Key id)
 */
protected Long declarationInvoiceInfo;
		 
/**
 * Attribute Name: Declaration Info Id(Foreign Key id)
 *  */

protected Long declarationInfoId;	
	
protected Integer sequenceNo;

protected String invoiceNo;

protected String invoiceDate;

protected String incotermCode;

protected BigDecimal invoiceValueForiegnCurrency;

protected String currencyCode;

protected String currencyRate;

protected String ruleCode;

protected BigDecimal insuranceCost;

protected BigDecimal frieghtCost;

protected BigDecimal totalCIFLocalCurrency;


   
}
