package com.dutech.ai.prh.api.v1.model.dto;

import com.dutech.ai.prh.dao.entity.RiskAssDeclaration;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class DeclarationInfoResponseDTO {
	
	protected DeclarationInfo declarationInfo;
	protected RiskAssDeclaration riskAssDeclaration;

}
