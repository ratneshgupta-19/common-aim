package com.dutech.ai.prh.api.v1.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DesResponseDto {
	
	private List<DesDeclarationResultListDto> resultList;

}
