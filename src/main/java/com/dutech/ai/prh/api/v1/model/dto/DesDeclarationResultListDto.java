package com.dutech.ai.prh.api.v1.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DesDeclarationResultListDto {
	
	@JsonProperty("TRS")
	private String trs;
	private String ruleId;
	private String category;
	
	@JsonProperty("indicator")
	private String riskIndicator;
	
	@JsonProperty("rule")
	private String ruleDetails;
	
	private Integer score;
	
	@JsonProperty("type")
	private String riskType;
	
	@JsonProperty("action")
	private String recomendedAction;
	
	@JsonProperty("ruleOwner")
	private String riskRuleOwner;
	
	@JsonProperty("insCaseAction")
	private String inspectionCaseAction;
	
	private String ruleCode;
	
	@JsonProperty("thresholdscore")
	private Integer thresholdCode;
	
	private boolean invoice;
	private boolean items; 
	
	

}
