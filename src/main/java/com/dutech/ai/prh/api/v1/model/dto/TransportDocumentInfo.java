package com.dutech.ai.prh.api.v1.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Harihara Sudhan
 */
@Data
@NoArgsConstructor
@ApiModel(description = "TransportDocumentInfo")

@JsonIgnoreProperties(ignoreUnknown = true)

public class TransportDocumentInfo  {
	
/**
 * * Attribute Name: Shipping Info Id(Foreign Key id)
 */
protected long shippingInfoId;
		 
/**
 * 
 * Attribute Name: TransportDocumentInfo(Primary Key id)
 */

protected long transportDocumentInfoId;

protected int sequenceNo;

protected String documentType;

protected String cargoType;

protected String marksNo;

protected BigDecimal netWeight;

protected BigDecimal grossWeight;

protected BigDecimal totalPackages;

protected int totalNumberOfContainers;

protected String uniqueConsignmentRefNo;

protected List<DeclarationContainerInfo> DeclarationContainerInfoList;

   
}
