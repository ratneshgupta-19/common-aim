package com.dutech.ai.prh.api.v1.model.dto;


import java.util.Date;
import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author Harihara Sudhan
 */


@Data
@NoArgsConstructor
@ApiModel(description = "DeclarationInfo")
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)

public class DeclarationInfo {
	


    /**
    * UID: 2380
    * 
    * Attribute Name: Document Cancel date
    
    */
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "Date and time when the document/message was cancelled")
    protected Date cancellationDateTime;

    /**
    * UID: 2380
    * 
    * Attribute Name: Document Effective Date
    
    */
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "The effective date of the document (e.g. license, visa, permit, certificate)")
    protected Date effectiveDateTime;
    
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "The acceptance date of the document (e.g. license, visa, permit, certificate)")
	protected Date acceptanceDateTime;

    /**
    * UID: 2380
    * 
    * Attribute Name: Document Expiry Date
    
    */
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "The expiry date of the document (e.g. license, visa, permit, certificate)")
    protected Date expirationDateTime;

    /**
    * UID: 1154
    * 
    * Attribute Name: Submitter Reference Number/Client Reference Number
    
    */
    @Size(min = 0, max = 70, message = "{Size.declaration.functionalReferenceID}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.functionalReferenceID}")
    @ApiModelProperty(notes = "Reference number identifying a specific information exchange")
    protected String clientReferenceNumber;

    /**
    * UID: 1004
    * 
    * Attribute Name: Declaration Number
    
    */
   // 
    @Size(min = 0, max = 70, message = "{Size.declaration.identifier}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.identifier}")
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "Identifier of a document providing additional information")
     protected String declarationNumber;

    
    
    /**
    * UID: 2380
    * 
    * Attribute Name: Document Issue Date
    
    */
    //
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "Date at which an additional document was issued and when appropriate, signed or otherwise authenticated")
    protected Date declarationIssueDateTime;

    /**
    * UID: 3411
    * 
    * Attribute Name: Document issue location
    * 
    * Code List: UN/LOCODE
    */
    
    @Size(min = 0, max = 5, message = "{Size.declaration.issueLocationID}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.issueLocationID}")
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "Place at which an additional document was issued and when appropriate, signed or otherwise authenticated")
    protected String declarationIssueLocationID;

    /**
    * UID: 3453
    * 
    * Attribute Name: Language
    * 
    * Code List: EDIFACT codes (3453)
    */
    
    @Size(min = 0, max = 3, message = "{Size.declaration.languageCode}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.languageCode}")
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "To identify a language used or requested")
    protected String languageCode;

    /**
    * UID: 2380
    * 
    * Attribute Name: Document Rejection Date
    
    */
    
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "Date and time when the document/message was rejected")
    protected Date rejectionDateTime;

    /**
    * UID: 7365
    * 
    * Attribute Name: Declaration Type
    * 
    * Code List: User codes
    */
    @Size(min = 0, max = 3, message = "{Size.declaration.specificCircumstancesCode}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.specificCircumstancesCode}")
    @ApiModelProperty(notes = "Code specifying circumstances for declarations")
    protected String declarationType;

    /**
    * UID: 1373
    * 
    * Attribute Name: Document Status
    * 
    * Code List: EDIFACT codes (1373)
    */
    
    @Size(min = 0, max = 3, message = "{Size.declaration.statusCode}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.statusCode}")
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "Code specifying the status of a document")
    protected String documentStatus;

    /**
    * UID: 3225
    * 
    * Attribute Name: Customs Office
    * 
    * Code List: UN/LOCODE (an..5) + user code (an..12), or user code (an..17)
    */
    @Size(min = 0, max = 17, message = "{Size.declaration.subsequentDeclarationOfficeID}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.subsequentDeclarationOfficeID}")
    @ApiModelProperty(notes = "To identify a Customs office location of Duty/ Tax payment at which a subsequent declaration is lodged")
    protected String customsOfficeCode;

    /**
    * UID: 8323
    * 
    * Attribute Name: Regime Type
    * 
    * Code List: EDIFACT codes (8323)
    */
    @Size(min = 0, max = 3, message = "{Size.declaration.transportMovementCode}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.transportMovementCode}")
    @ApiModelProperty(notes = "Code specifying a particular transport movement")
    protected String regimeType;



    /**
    * UID: 1057
    * 
    * Attribute Name: Document Version number
    
    */
    
    @Size(min = 0, max = 9, message = "{Size.declaration.versionID}")
    @Pattern(regexp = "^[a-zA-Z0-9_\\- ]*$", message = "{Pattern.alphanumerical.declaration.versionID}")
    @ApiModelProperty(accessMode = AccessMode.READ_ONLY, notes = "The sequential number that determines the version of a specific document/message number")
    protected String documentVersionNo;
    
    protected List<SupportingDocumentInfo> supportingDocumentList;
    
    protected String transportMode;
    
    protected String declarantCode;
    
    protected Long declarationId;
    
    protected String declarantName;
    
    protected String submitterCode;
    
    protected String submitterName;
    
    protected String submitterRemarks;
    
    protected String reviewerCode;
    
    protected String reviewerName;
    
    protected String reviewerRemarks;
    
    protected String brokerCode;
    
    protected String brokerName;
    
    protected String payMethodCode;
    
    protected String payMethodName;

	protected String overallStatus;
    protected String remarks;
	protected String further_action;
    protected ShippingInfo shippingInfo;
    
    protected List<DeclarationInvoiceInfo> declarationInvoiceInfoList;

    protected String importerCode;
    
    protected String importerName;
    
    protected String exporterCode;
    
    protected String exporterName;
    
    protected List<GoodDetailsInfo> goodDetailsInfoList;
  
    
}



