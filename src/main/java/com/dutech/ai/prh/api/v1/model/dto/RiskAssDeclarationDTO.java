package com.dutech.ai.prh.api.v1.model.dto;



import java.time.LocalDate;
import java.util.List;
import com.dutech.ai.prh.dao.entity.RiskAssProfile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;


@ApiModel(description = "AEO Request Header Model")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class RiskAssDeclarationDTO{
	
	@ApiModelProperty
	private Integer riskAssId;
	@ApiModelProperty
	private String referenceCode;
	@ApiModelProperty
	private Long referanceId;
	@ApiModelProperty
	private String importer;
	@ApiModelProperty
	private String AgentCode;
	@ApiModelProperty
	private String Status;
	@ApiModelProperty
	private String regime;
	@ApiModelProperty
	private String procedure;
	@ApiModelProperty
	private String modeOfTransport;
	@ApiModelProperty
	private Integer noOfLines;
	@ApiModelProperty
	private Integer noOfContainers;
	@ApiModelProperty
	private String goodsLocation;
	@ApiModelProperty
	private LocalDate dateOfarrival;
	@ApiModelProperty
	List<RiskAssProfile> riskAssProfileList;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String noInspection;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String documentVerification;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String physicalInspection;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String samplingInspection;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String postClearanceAudit;
	
}
