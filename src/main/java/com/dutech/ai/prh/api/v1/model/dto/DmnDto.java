package com.dutech.ai.prh.api.v1.model.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.dutech.ai.prh.api.v1.model.dto.DmnDto.DefinitionsDto.DecisionDto.DecisionTableDto.InputDto.InputExpressionDto.TextDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "Input Decision model for generate the dmn")
public class DmnDto {
    
    protected DefinitionsDto dmnDefinitions;
    
    @Data
    @NoArgsConstructor
    public static class DefinitionsDto {
        @NotNull
        private String id;
        //private String name;
        protected List<DecisionDto> dmnDecisions;

        @Data
        @NoArgsConstructor
        public static class DecisionDto {
            @NotNull
            @ApiParam(value ="This id used for evaluate the decision for dmn service and it is unique")
            private String id;
            @NotNull
            @ApiParam(value ="Name of the decision")
            private String name;
            protected DecisionTableDto dmnDecisionTable;

            @Data
            @NoArgsConstructor
            public static class DecisionTableDto {
                @NotNull
                @ApiParam(value ="unique name of decision table , if any")
                private String id;
                
                @ApiParam(value ="Possible values are UNIQUE,FIRST,PRIORITY,ANY,COLLECT")
                protected String dmnHitPolicy;
                
                @NotNull
                protected List<InputDto> dmnInputs;
                @NotNull
                protected List<OutputDto> dmnOutputs;
                @NotNull
                protected List<RuleDto> dmnRules;

                @Data
                @NoArgsConstructor
                public static class InputDto {
                    @NotNull
                    @ApiParam(value ="Name of the input indicator , this used to give meaning full info about the indicator")
                    private String label;
                    @NotNull
                    protected InputExpressionDto dmnInputExpression;

                    @Data
                    @NoArgsConstructor
                    public static class InputExpressionDto {
                        @NotNull
                        @ApiParam(value ="this data type of indicators and possible values are string,boolean,integer,long,double,date")
                        private String typeRef;
                        protected TextDto dmnText;

                        @Data
                        @NoArgsConstructor
                        public static class TextDto {
                            @NotNull
                            @ApiParam(value ="We need to pass the exact indicator name and will use at during the evaluete the decision")
                            private String textContent;                            
                        }
                    }
                }
                
                @Data
                @NoArgsConstructor
                public static class OutputDto {    
                    @NotNull
                    @ApiParam(value ="Name of the output indicator , this used to give meaning full info about the indicator")
                    private String label;
                    @NotNull
                    @ApiParam(value ="We need to pass the exact indicator name ")
                    private String name;
                    @NotNull
                    @ApiParam(value ="this data type of indicators and possible values are string,boolean,integer,long,double,date")
                    private String typeRef;                    
                }

                @Data
                @NoArgsConstructor
                public static class RuleDto {
                    @NotNull
                    protected List<InputEntryDto> dmnInputEntries;
                    @NotNull
                    protected List<OutputEntryDto> dmnOutputEntries;

                    @Data
                    @NoArgsConstructor
                    public static class InputEntryDto {
                        @NotNull
                        protected TextDto inputText;
                    }

                    @Data
                    @NoArgsConstructor
                    public static class OutputEntryDto {
                        @NotNull
                        protected TextDto outputText;
                    }
                }
            }
        }
    }
}
