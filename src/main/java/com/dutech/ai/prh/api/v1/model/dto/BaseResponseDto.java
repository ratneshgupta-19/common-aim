package com.dutech.ai.prh.api.v1.model.dto;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.spi.ErrorMessage;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Response Format")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BaseResponseDto<T> {
	private Integer statusCode;
	private String message;
	@Builder.Default
	private List<ErrorMessage> errorMessages=new ArrayList<>();
	private T content ;
	private T responseDto ;
}