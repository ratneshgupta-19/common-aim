package com.dutech.ai.prh.api.v1.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DesItemResultListDto {
	
	private boolean output;

}
