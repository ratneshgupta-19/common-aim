package com.dutech.ai.prh.service;

import java.util.List;

import com.dutech.ai.prh.api.v1.model.dto.DeclarationInfo;
import com.dutech.ai.prh.api.v1.model.dto.RiskAssDeclarationDTO;
import com.dutech.ai.prh.dao.entity.RiskAssDmnContent;

/**
 * Service interface for managing Risk Assessments.
 * 
 * @author Bojjanna.Vemula
 */
public interface RiskAssessmentService {

	/**
	 * Risk Assessment.
	 * 
	 * @param request - input data for risk assessment 
	 * @return The Risk Assessment result
	 */
	
	String riskAssessment(Integer declarationId);
	
	public List<RiskAssDmnContent> getRiskAssDmnContent();
	
	RiskAssDeclarationDTO calculateRiskAssDeclarationSync(Integer declarationId) throws Exception;
	DeclarationInfo declarationInfoFromDecl(Integer declarationId);

	public RiskAssDeclarationDTO calculateRiskAssDeclSync(DeclarationInfo request);

}
