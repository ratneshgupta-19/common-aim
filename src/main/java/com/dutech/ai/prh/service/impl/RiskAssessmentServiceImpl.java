package com.dutech.ai.prh.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.dutech.ai.prh.ApplicationConstants;
import com.dutech.ai.prh.api.v1.model.dto.DeclarationInfo;
import com.dutech.ai.prh.api.v1.model.dto.DeclarationInfoResponseDTO;
import com.dutech.ai.prh.api.v1.model.dto.RiskAssDeclarationDTO;
import com.dutech.ai.prh.dao.entity.DeclarationInfoEntity;
import com.dutech.ai.prh.dao.entity.RiskAssDeclaration;
import com.dutech.ai.prh.dao.entity.RiskAssDmnContent;
import com.dutech.ai.prh.dao.entity.ShippingInfoEntity;
import com.dutech.ai.prh.dao.repository.DeclarationInfoRepository;
import com.dutech.ai.prh.helper.RiskAssessmentHelper;
import com.dutech.ai.prh.service.RiskAssessmentService;
import com.dutech.ai.prh.utils.CommonUtil;
import com.dutech.ai.prh.utils.ErrorCode;
import com.dutech.ai.prh.utils.ErrorMessage;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Bojjanna.Vemula
 *
 */
@Service
@Slf4j
public class RiskAssessmentServiceImpl implements RiskAssessmentService {

	@Autowired
	private RiskAssessmentHelper riskAssessmentHelper;

	@Autowired
	DeclarationInfoRepository declarationInfoRepository;
	
    @Autowired
    private ModelMapper mapper;	
	

	private void checkInspectionCaseAction(RiskAssDeclarationDTO riskAssDeclarationDto, String inspection) {
		String inspectionAction=inspection.trim();
		if(ApplicationConstants.INSPECTION_ACTION_NA.equalsIgnoreCase(inspectionAction)) {
			riskAssDeclarationDto.setNoInspection("YES");
		}
		if(ApplicationConstants.INSPECTION_ACTION_PI.equalsIgnoreCase(inspectionAction)) {
			riskAssDeclarationDto.setPhysicalInspection("YES");
		}
		if(ApplicationConstants.INSPECTION_ACTION_DV.equalsIgnoreCase(inspectionAction)) {
			riskAssDeclarationDto.setDocumentVerification("YES");
		}
		if(ApplicationConstants.INSPECTION_ACTION_SI.equalsIgnoreCase(inspectionAction)) {
			riskAssDeclarationDto.setSamplingInspection("YES");
		}
		if(ApplicationConstants.INSPECTION_ACTION_PCA.equalsIgnoreCase(inspectionAction)) {
			riskAssDeclarationDto.setPostClearanceAudit("YES");
		}
	}
	
	@Override
	public RiskAssDeclarationDTO calculateRiskAssDeclarationSync(Integer declarationId) throws Exception {
		log.info("Start the RISK ASS Sync Process");
		try {
			DeclarationInfo declarationInfo=null;
			Optional<DeclarationInfoEntity>  declRes = declarationInfoRepository.findByDeclarationId(declarationId);
			if(declRes.isPresent()) {
				log.info("saved declation info object:::"+declRes.toString());
				 declarationInfo = mapper.map(declRes.get(), DeclarationInfo.class);
				log.info("mapped declation info object:::"+declarationInfo.toString());
			}else {
				throw new Exception("something went wrong in mapping");
			}
			
			DeclarationInfoResponseDTO declarationInfoResponseDTO = mappingRiskAssDeclaration(declarationInfo);
			
			RiskAssDeclaration riskAssessDeclarationEntity=riskAssessmentHelper.calculateRiskAssDeclarationSync(declarationInfoResponseDTO.getDeclarationInfo(),
				declarationInfoResponseDTO.getRiskAssDeclaration());
			
			RiskAssDeclarationDTO riskAssDeclarationDto= mapper.map(riskAssessDeclarationEntity, RiskAssDeclarationDTO.class);
			String inspectionAction=	riskAssessDeclarationEntity.getInspectionAction();
			
			if(inspectionAction.contains(",")) {
				String inspectionArr[]=inspectionAction.split(",");
				
				for(String inspectionCaseAction:inspectionArr) {
					checkInspectionCaseAction(riskAssDeclarationDto, inspectionCaseAction);
				}
			}else {
				checkInspectionCaseAction(riskAssDeclarationDto, inspectionAction);
			}
				
		
		return riskAssDeclarationDto;
			
		}catch (DataAccessException ex) {
			log.error("Exception occoured while saving data for declaration object in risk assessment :", ex);
			ErrorMessage errMessage = CommonUtil.createErrorMessage(ErrorCode.VPRECORDNOTSAVED.getCode(),
					ErrorCode.VPRECORDNOTSAVED.getShortDescription(), ex.toString());
			throw new Exception(errMessage.toString());
		}
		catch (Exception ex) {
			log.error("Exception occoured while assess the risk assessment:", ex);
			ErrorMessage errMessage = CommonUtil.createErrorMessage(ErrorCode.VPINTERNALSERVERERROR.getCode(),
					ErrorCode.VPINTERNALSERVERERROR.getShortDescription(), ex.toString());
			throw new Exception(errMessage.toString());
		}
	}
	

	private DeclarationInfoResponseDTO mappingRiskAssDeclaration(DeclarationInfo request){
		DeclarationInfoResponseDTO declarationInfoResponseDTO = new DeclarationInfoResponseDTO();
		declarationInfoResponseDTO.setDeclarationInfo(request);
		RiskAssDeclaration riskAssDeclaration = new RiskAssDeclaration();
		riskAssDeclaration.setAgentCode(request.getDeclarantCode());
		riskAssDeclaration.setCreatedBy(ApplicationConstants.CREATEDBY);
		riskAssDeclaration.setCreatedDate(LocalDate.now());
		riskAssDeclaration.setDateOfarrival(CommonUtil.DateToLocalDate(request.getShippingInfo().getArrivalDate()));
		riskAssDeclaration.setGoodsLocation(request.getShippingInfo().getDestinationPortName());
		riskAssDeclaration.setImporter(request.getDeclarantCode());
		if(null != request.getShippingInfo()) {
		riskAssDeclaration.setModeOfTransport( request.getShippingInfo().getVesselRoutingInfoList().size() >=1?  request.getShippingInfo().getVesselRoutingInfoList().get(0).getPortType() : null);
		riskAssDeclaration.setNoOfContainers(null != request.getShippingInfo().getTranportDocumentInfoList() ? request.getShippingInfo().getTranportDocumentInfoList().stream().mapToInt(transDoc -> transDoc.getDeclarationContainerInfoList().size()).sum() : 0);
		}
		riskAssDeclaration.setNoOfLines(request.getGoodDetailsInfoList() !=null? request.getGoodDetailsInfoList().size():0);
		
		riskAssDeclaration.setProcedure(request.getDeclarationType());
		riskAssDeclaration.setReferanceId(request.getDeclarationId());
		riskAssDeclaration.setRegime(request.getRegimeType());
		riskAssDeclaration.setStatus(ApplicationConstants.RISK_ASS_STATUS);
		riskAssDeclaration.setReferenceCode(request.getDeclarationNumber());
		//riskAssDeclaration=riskAssDeclarationRepository.saveAndFlush(riskAssDeclaration);
		declarationInfoResponseDTO.setRiskAssDeclaration(riskAssDeclaration);
		return declarationInfoResponseDTO;
	}

	@Override
	public String riskAssessment(Integer declarationId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RiskAssDmnContent> getRiskAssDmnContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeclarationInfo declarationInfoFromDecl(Integer declarationId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RiskAssDeclarationDTO calculateRiskAssDeclSync(DeclarationInfo request) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
