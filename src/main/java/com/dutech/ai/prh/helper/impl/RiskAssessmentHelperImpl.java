/**
 * 
 */
package com.dutech.ai.prh.helper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dutech.ai.prh.ApplicationConstants;
import com.dutech.ai.prh.api.v1.model.dto.DeclarationInfo;
import com.dutech.ai.prh.api.v1.model.dto.DeclarationInvoiceInfo;
import com.dutech.ai.prh.api.v1.model.dto.DesDeclarationResultListDto;
import com.dutech.ai.prh.api.v1.model.dto.DesInvoiceResponseDto;
import com.dutech.ai.prh.api.v1.model.dto.DesInvoiceResultListDto;
import com.dutech.ai.prh.api.v1.model.dto.DesItemResponseDto;
import com.dutech.ai.prh.api.v1.model.dto.DesItemResultListDto;
import com.dutech.ai.prh.api.v1.model.dto.DesResponseDto;
import com.dutech.ai.prh.api.v1.model.dto.GoodDetailsInfo;
import com.dutech.ai.prh.api.v1.model.dto.TrsEnum;
import com.dutech.ai.prh.dao.entity.RiskAssDeclaration;
import com.dutech.ai.prh.dao.entity.RiskAssDeclarationThreatSummary;
import com.dutech.ai.prh.dao.entity.RiskAssItemDetails;
import com.dutech.ai.prh.dao.entity.RiskAssProfile;
import com.dutech.ai.prh.dao.repository.RiskAssDeclarationRepository;
import com.dutech.ai.prh.helper.RiskAssessmentHelper;
import com.dutech.ai.prh.support.client.services.des.domain.DesResponse;
import com.dutech.ai.prh.support.client.services.des.service.DesService;
import com.dutech.ai.prh.utils.CommonUtil;
import com.dutech.ai.prh.utils.ErrorCode;
import com.dutech.ai.prh.utils.ErrorMessage;
import com.dutech.ai.prh.utils.ModelMapperUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Bojjanna.Vemula
 *
 */
@Service
@Slf4j
public class RiskAssessmentHelperImpl implements RiskAssessmentHelper {

	@Autowired
	private DesService desService;	
	
	 @Autowired
	 private RiskAssDeclarationRepository riskAssDeclarationRepository;
	
	 
	 @Override
	 public RiskAssDeclaration calculateRiskAssDeclarationSync(DeclarationInfo request, RiskAssDeclaration riskAssDeclaration) throws Exception {
		  
		log.info("Start process the Risk Assessment for Request " + asJsonString(request));
		RiskAssDeclaration riskAssessDeclarationEntity = null;
		DesResponse desResponse = desService.evaluateData(request, ApplicationConstants.DECLARATION_DECISION_KEY);
		List<Map<String, Object>> ruleResult = desResponse.getResultList();
		log.info("parseDeclaration ruleResult  " + ruleResult);

		Map<String, List<GoodDetailsInfo>> resultMap = new HashMap<>();

		String responseStr = asJsonString(desResponse);

		DesResponseDto responseDto = convertJsonToObject(responseStr, DesResponseDto.class);

		List<DesDeclarationResultListDto> desResultListDto = responseDto.getResultList();

		riskAssDeclaration.setInspectionAction("NA");
		
		if (!CollectionUtils.isEmpty(desResultListDto)) {

			evaluateRiskAssessment(request, resultMap, desResultListDto);

			log.debug("list of profiles matche with dmn::::::" + resultMap);

			List<RiskAssProfile> riskAssProfileList = filterItemToSave(resultMap, desResultListDto);
			riskAssDeclaration.setRiskAssProfileList(riskAssProfileList);

			// logic for group by TRS
			if(!CollectionUtils.isEmpty(riskAssProfileList)) {
			Map<String, Integer> trsMapWithScore = riskAssProfileList.stream().collect(
					Collectors.groupingBy(RiskAssProfile::getTrs, Collectors.summingInt(RiskAssProfile::getScore)));

			List<RiskAssDeclarationThreatSummary> threatSummaryList = createRiskThreatSummary(riskAssDeclaration,
					riskAssProfileList, trsMapWithScore);

			riskAssDeclaration.setRiskAssDeclarationThreatSummary(threatSummaryList);
			

			Set<String> inspectionSet= new HashSet<>();
			
			riskAssProfileList.forEach(res->{
				String inspectionCaseAction= res.getInspectionCaseAction();
				
				if(inspectionCaseAction.contains("/")) {
					String inspectionArr[]=inspectionCaseAction.split("/");
					for(String inspectionAction:inspectionArr) {
						inspectionSet.add(inspectionAction);
					}
				}else {
					inspectionSet.add(inspectionCaseAction);
				}
			});
			//String.join(", ", inspectionSet);
			riskAssDeclaration.setInspectionAction(String.join(", ", inspectionSet));
			}
		} else {
			//riskAssDeclaration.setInspectionAction("NA");
			log.info("No matching risk profile found for submitted declaration.");
		}
		try {
			riskAssDeclaration.setStatus(ApplicationConstants.RISK_ASS_STATUS_COMPLETED);
			riskAssessDeclarationEntity = riskAssDeclarationRepository.save(riskAssDeclaration);
		} catch (DataAccessException ex) {

			log.error("Error while saving the declaration with risk assessment profile" + ex.getMessage());
			ErrorMessage errMessage = CommonUtil.createErrorMessage(ErrorCode.VPRECORDNOTSAVED.getCode(),
					ErrorCode.VPRECORDNOTSAVED.getShortDescription(), ex.toString());
			throw new Exception(errMessage.toString());
		}
		return riskAssessDeclarationEntity;
		}
	
	
	/**
	 * This method is used to evaluate the risk threat summary logic.
	 * @param riskDeclaration
	 * @param riskAssProfileList
	 * @param trsMapWithScore
	 * @return
	 */
	private List<RiskAssDeclarationThreatSummary> createRiskThreatSummary(RiskAssDeclaration riskDeclaration,
			List<RiskAssProfile> riskAssProfileList, Map<String, Integer> trsMapWithScore) {
		List<RiskAssDeclarationThreatSummary> threatSummaryList= new ArrayList<>();
		trsMapWithScore.forEach((key,value)->{
			
			if(key.equalsIgnoreCase(TrsEnum.IPR.toString())) {
				
				riskDeclaration.setIpr(value.intValue());
				
			}if(key.equalsIgnoreCase(TrsEnum.REV.toString())) {
				
				riskDeclaration.setRev(value.intValue());
			}
			if(key.equalsIgnoreCase(TrsEnum.RANDOM.toString())) {
				
				riskDeclaration.setRandom(value.intValue());
				
			}
			if(key.equalsIgnoreCase(TrsEnum.VAL.toString())) {
				
				riskDeclaration.setVal(value.intValue());
				
			}
			
			RiskAssDeclarationThreatSummary riskThreat= new RiskAssDeclarationThreatSummary();
			riskThreat.setLevel("-99");
			riskThreat.setName(key);
			riskThreat.setScore(value);
			if(riskAssProfileList.stream().anyMatch(rs-> rs.getTrs().equalsIgnoreCase(key)
					&& rs.getRiskType().equalsIgnoreCase("Deductive")))
			riskThreat.setDeductive(true);
			else
				riskThreat.setDeductive(false);
			
			threatSummaryList.add(riskThreat);
		});
		return threatSummaryList;
	}

	/**
	 * This method is used to filter the items match with the dmn item output.
	 * @param resultMap
	 * @param desResultListDto
	 * @return
	 */
	private List<RiskAssProfile> filterItemToSave(Map<String, List<GoodDetailsInfo>> resultMap,
			List<DesDeclarationResultListDto> desResultListDto) {
		
		List<RiskAssProfile> riskAssProfileList= new ArrayList<>();
		
		desResultListDto.stream().forEach(dmnriskProfile -> {

			if (resultMap.containsKey(dmnriskProfile.getRuleCode())) {

				RiskAssProfile riskAssProfile = ModelMapperUtils.map(dmnriskProfile, RiskAssProfile.class);

				List<GoodDetailsInfo> goodsInfoList = resultMap.get(dmnriskProfile.getRuleCode());

				if (!CollectionUtils.isEmpty(goodsInfoList)) {
					List<RiskAssItemDetails> riskAssItemsDetailsList= new ArrayList<>();
					for (GoodDetailsInfo goodsDetails : goodsInfoList) {
						RiskAssItemDetails itemDetails = new RiskAssItemDetails();
						itemDetails.setComodityCode(goodsDetails.getHsCode());
						itemDetails.setDescription(goodsDetails.getGoodsDescription());
						itemDetails.setSequenceNo(Long.valueOf(goodsDetails.getSequenceNo()));
						itemDetails.setType(riskAssProfile.getTrs());
						itemDetails.setScore(riskAssProfile.getScore());
						riskAssItemsDetailsList.add(itemDetails);
					}
					riskAssProfile.setRiskAssItemsDetailsList(riskAssItemsDetailsList);
					
					int finalScore=riskAssProfile.getScore() *
							riskAssItemsDetailsList.size();
					
					if(finalScore < riskAssProfile.getThresholdCode()) {
					
					riskAssProfile.setFinalScore(finalScore);
					}else {
						riskAssProfile.setFinalScore(riskAssProfile.getThresholdCode());
					}
				}else {
					riskAssProfile.setFinalScore(riskAssProfile.getScore());
				}
				riskAssProfileList.add(riskAssProfile);
			}
			
		});
		return riskAssProfileList;
	}

	/**
	 * This method is used to match the rule from dmn.
	 * @param declarationInfo
	 * @param resultMap
	 * @param desResultListDto
	 */

	private void evaluateRiskAssessment(DeclarationInfo declarationInfo, Map<String, List<GoodDetailsInfo>> resultMap,
			List<DesDeclarationResultListDto> desResultListDto) {
		
		for (DesDeclarationResultListDto res : desResultListDto) {

			List<GoodDetailsInfo> finalItemList = new ArrayList<>();
			String profileCode = res.getRuleCode();
				
			if (Boolean.TRUE.equals(res.isItems())) {

				List<GoodDetailsInfo> itemList = declarationInfo.getGoodDetailsInfoList();

				if (!CollectionUtils.isEmpty(itemList)) {

					itemList.forEach(item -> {

						item.setRuleCode(profileCode);

						DesResponse itemResponse = desService.evaluateData(item,
								ApplicationConstants.ITEM_DECISION_KEY);

						List<Map<String, Object>> itemResult = itemResponse.getResultList();
						log.debug("parse item ruleResult  " + itemResult);

						String itemResponseStr = asJsonString(itemResponse);

						DesItemResponseDto responseItemDto = convertJsonToObject(itemResponseStr,
								DesItemResponseDto.class);

						List<DesItemResultListDto> itemResultList = responseItemDto.getResultList();

						if (!CollectionUtils.isEmpty(itemResultList)) {
							finalItemList.add(item);
						}
					});
				}

				if (!CollectionUtils.isEmpty(finalItemList)) {
					resultMap.put(profileCode, finalItemList);
				} else {
					continue;
				}

			}

			if (Boolean.TRUE.equals(res.isInvoice())) {

				List<DeclarationInvoiceInfo> invoiceList = declarationInfo.getDeclarationInvoiceInfoList();
				boolean isInvoiceMatched = false;

				if (!CollectionUtils.isEmpty(invoiceList)) {

					for (DeclarationInvoiceInfo invoice : invoiceList) {
						invoice.setRuleCode(profileCode);

						DesResponse invoiceResponse = desService.evaluateData(invoice,
								ApplicationConstants.INVOICE_DECISION_KEY);

						List<Map<String, Object>> invoiceResult = invoiceResponse.getResultList();

						log.debug("parse invoice ruleResult  " + invoiceResult);

						String parsedInvoiceResponseStr = asJsonString(invoiceResponse);

						DesInvoiceResponseDto responseInvoiceDto = convertJsonToObject(parsedInvoiceResponseStr,
								DesInvoiceResponseDto.class);

						List<DesInvoiceResultListDto> invoiceResultList = responseInvoiceDto.getResultList();

						if (!CollectionUtils.isEmpty(invoiceResultList)) {
							isInvoiceMatched = true;
							break;

						}

					}

					if (!isInvoiceMatched) {
						resultMap.remove(profileCode);
					}

					if (isInvoiceMatched && !resultMap.containsKey(profileCode)) {
						resultMap.put(profileCode, null);
					}
				}
			}
			
			if (Boolean.FALSE.equals(res.isInvoice()) && Boolean.FALSE.equals(res.isItems())) {
				
				resultMap.put(profileCode, null);
			} 

		}
	}
	
	
	/**
	 * This method is used to read a file from classpath and convert it to respective class
	 * @param <T>
	 * @param fileName
	 * @param classname
	 * @return
	 */
	private <T> T convertJsonToObject(String jsonData, Class<T> classname) {
		T className = null;
		try {
			
			  ObjectMapper objectMapper = new ObjectMapper();
			  objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
			  false); 
			  objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES,
					  true); 
			  
			className = objectMapper.readValue(jsonData, classname);
		} catch (Exception ex) {
			System.out.println("error while parsing json to object " + ex.getMessage());

		}
		return className;

	}
	
	/**
	 * This method is used to parse an object to string.
	 * @param obj
	 * @return
	 */
	private String asJsonString(final Object obj) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
			objectMapper.registerModule(new JavaTimeModule());

			return objectMapper.writeValueAsString(obj);
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}


	@Override
	public void calculateDecRisk(DeclarationInfo request, RiskAssDeclaration riskAssDeclaration) {
		// TODO Auto-generated method stub
		
	}

}
