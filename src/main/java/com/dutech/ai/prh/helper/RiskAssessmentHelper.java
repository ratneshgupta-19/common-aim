/**
 * 
 */
package com.dutech.ai.prh.helper;

import com.dutech.ai.prh.api.v1.model.dto.DeclarationInfo;
import com.dutech.ai.prh.dao.entity.RiskAssDeclaration;

/**
 * @author Bojjanna.Vemula
 *
 */
public interface RiskAssessmentHelper {


	void calculateDecRisk(DeclarationInfo request, RiskAssDeclaration riskAssDeclaration);
	
	RiskAssDeclaration calculateRiskAssDeclarationSync(DeclarationInfo request, RiskAssDeclaration riskAssDeclaration) throws Exception;

}
