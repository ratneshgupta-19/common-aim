CREATE SCHEMA IF NOT EXISTS prh_schema;
CREATE SEQUENCE IF NOT EXISTS prh_schema.hibernate_sequence;
CREATE SCHEMA IF NOT EXISTS prh_audit_schema;
CREATE SEQUENCE IF NOT EXISTS prh_audit_schema.hibernate_sequence;